import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {
  @Input() arraydelpadre: any;
  @Input() textodelpadre: any;
  textoHijo: string;
  arrayHijo: number[];
  suma: number;

  constructor() {
    this.textoHijo='Texto hijo inicial';
    this.arrayHijo=[];
    this.suma=0;
  }

  ngOnInit() {
  }

  actualizarArrayHijo(){
    this.arrayHijo= this.arraydelpadre;
    this.suma=0;
    if(this.arrayHijo != null){
      for(let i=0; i<this.arrayHijo.length; i++){
        this.suma += this.arrayHijo[i];
      }
    }
  }

  actualizarTextoHijo(){
    this.textoHijo= this.textodelpadre;
  }

}
