import { Component, OnInit, ViewChild } from '@angular/core';
import { HijoComponent } from '../hijo/hijo.component';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
  @ViewChild('datosDesdeElPadre', { static: false }) datosDesdeElPadre: HijoComponent;
  arrayPadre: number[];
  textoPadre: string;

  constructor() {
    this.arrayPadre=[];
    this.textoPadre='';
  }

  ngOnInit() {}

  optionChanged(selectedValue: string){
    this.textoPadre=selectedValue;
    this.datosDesdeElPadre.textodelpadre = this.textoPadre;
    this.datosDesdeElPadre.actualizarTextoHijo();
  }

  arrayChanged(){
    this.arrayPadre=[1,2,3,4,5,6,7,8,9,10];
    this.datosDesdeElPadre.arraydelpadre = this.arrayPadre;
    this.datosDesdeElPadre.actualizarArrayHijo();
  }

}
